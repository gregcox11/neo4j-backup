#!/usr/bin/env python
import os
import pysystemd
import shutil
import errno
import datetime
import sys
import boto3
from ec2_metadata import ec2_metadata
import requests
import json
from argparse import RawTextHelpFormatter
from argparse import ArgumentParser
import time
import pwd
import grp

region_name = ec2_metadata.region
role = requests.get("http://169.254.169.254/latest/meta-data/iam/security-credentials")
resp = requests.get("http://169.254.169.254/latest/meta-data/iam/security-credentials/{}".format(role.content))
data = json.loads(resp.content)
ACCESS_KEY = data["AccessKeyId"]
SECRET_KEY = data["SecretAccessKey"]
TOKEN = data["Token"]

help_msg = '''
    python version >=2.7 is required.

    Examples: For help
    python neo4_backup.py -h

    Example execution:
    python neo4_backup.py
    python neo4_backup.py -a restore -r LATEST"
    python neo4_backup.py -a restore -r 20190726"
    python neo4_backup.py -a restore -r 20190726 -l local"
    python neo4_backup.py -a restore -r LATEST -l local"
    python neo4_backup.py -a restore -r 20190726 -l s3"
    python neo4_backup.py -a restore -r LATEST -l s3"
    python neo4_backup.py -a restore -r 20190726 -l snapshot"
    python neo4_backup.py -a restore -r LATEST -l snapshot"

    Notes:
    /etc/environment needs to have BACKUP_BUCKET=<bucket name>
    /etc/environment includes BACKUP_BUCKET as part of cloudformation build so should exist
'''

def get_bucket():
    f=open("/etc/environment", "r")
    content = f.readlines()
    for line in content:
        if "BACKUP_BUCKET" in line:
            bucket = line.split("=")
            return bucket[1].rstrip()

def upload_files(path):
    for subdir, dirs, files in os.walk(path):
        for file in files:
            full_path = os.path.join(subdir, file)
            with open(full_path, 'rb') as data:
                upload_to_aws(full_path, get_bucket(), full_path[len(path)+1:])
                # print("Upload Successful - {}".format(full_path))

def upload_to_aws(local_file, bucket, s3_file):
    s3 = boto3.client('s3', aws_access_key_id=ACCESS_KEY,
                      aws_secret_access_key=SECRET_KEY,
                      aws_session_token=TOKEN)
    s3.upload_file(local_file, bucket, s3_file)
    print("Upload Successful - {}".format(local_file))

def stopService(unit_name="neo4j"):
    try:
        print('stopping {} service'.format(unit_name))
        pysystemd.services('{}.service'.format(unit_name)).stop()
    except pysystemd.subprocess.CalledProcessError as err:
        print("Error stopping service: {}".format(err))
        os.sys.exit(1)
    # print('\nsuccessfully stopped {}\n'.format(unit_name))

def startService(unit_name="neo4j"):
    try:
        print('starting {} service'.format(unit_name))
        pysystemd.services('{}.service'.format(unit_name)).start()
    except pysystemd.subprocess.CalledProcessError as err:
        print("Error starting service: {}".format(err))
        os.sys.exit(1)
    # print('\nsuccessfully started {}\n'.format(unit_name))

def statusService(unit_name="neo4j"):
    try:
        status = pysystemd.status('{}.service'.format(unit_name)).is_run()
        # print('status = {}'.format(status))
        return status
    except pysystemd.subprocess.CalledProcessError as err:
        print("Error stopping service: {}".format(err))
        os.sys.exit(1)
    # print('\nsuccessfully stopped {}\n'.format(unit_name))

def getState(value):
    if value == 0:
        return 'running'
    else:
        return 'stopped'

def copyDirectory(src, dest):
    try:
        shutil.copytree(src, dest)
    except OSError as e:
        # If the error was caused because the source wasn't a directory
        if e.errno == errno.ENOTDIR:
            shutil.copy(src, dest)
        else:
            print('Directory not copied. Error: %s' % e)
            startService()
            sys.exit(1)

def initiateBackup(date, src):
    print('starting backup')
    dest = '/data/backup/{}/'.format(date.strftime("%Y%m%d"))
    copyDirectory(src, dest)
    print('backup complete')

def backup(svc, status, src):
    date = datetime.datetime.now()
    if status == 0:
        print('{} is {}'.format(svc, getState(0)))
        stopService()
        if statusService() == 1:
            print('{} is {}'.format(svc, getState(1)))
            initiateBackup(date, src)
            upload_files('/data/backup')
    else:
            initiateBackup(date, src)
            upload_files('/data/backup')

def copyDB(src, dest):
    try:
        copyDirectory(src, dest)
        return True
    except:
        print("Error while copying {} to {}".format(src, dest))
        sys.exit(1)

def findLatest(location):
    if location == "local":
        directories = []
        dir = os.listdir("/data/backup/")
        for i in dir:
            directories.append(int(i))
        return "/data/backup/{}".format(str(max(directories)))


def restoreFiles(version, location):
    db_path = "/data/neo4j/"
    uid = pwd.getpwnam("neo4j").pw_uid
    gid = grp.getgrnam("neo4j").gr_gid
    if version == "LATEST" and location == "local":
        bak_path = findLatest(location)
        try:
            print("Copying files from local backup to database location, /data/neo4j")
            copyDirectory(bak_path, db_path)
            os.chown(db_path, uid, gid)
        except:
            print("Error copying backed up files to database location, /data/neo4j")
    elif verson != "LATEST" and location == "local":
        bak_path = "/data/backup/{}/".format(version)
        try:
            print("Copying files from local backup to database location, /data/neo4j")
            copyDirectory(bak_path, db_path)
            os.chown(db_path, uid, gid)
        except:
            print("Error copying backed up files to database location, /data/neo4j")

def restore(svc, version, location):
    print("This restore process will move the existing database files to neo4j.bak in the /data/ directory.")
    while True:
        # Get input.
        value = raw_input("Would you like to proceed with the restore, Y or N:")

        # Break if user types Y or N.
        if value == "Y" or value == "y":
            break
        elif value == "N" or value == "n":
            print("Stopping execution")
            sys.exit(0)

    # Display value.
    print("Restore Starting")
    stopService()
    status = statusService()
    if getState(status) == "stopped":
        copyDB('/data/neo4j/', '/data/neo4j.bak/')
        try:
            print("Deleting /data/neo4j")
            shutil.rmtree('/data/neo4j')
        except:
           print('Error while deleting directory')
           sys.exit(1)
    else:
        stopService()
        copyDB('/data/neo4j/', '/data/neo4j.bak/')
        try:
            print("Deleting /data/neo4j")
            shutil.rmtree('/data/neo4j')
        except:
           print('Error while deleting directory')
           sys.exit(1)
    restoreFiles(version, location)

if __name__ == '__main__':

    parser = ArgumentParser(description=help_msg, formatter_class=RawTextHelpFormatter)
    action = parser.add_mutually_exclusive_group()
    action.add_argument("-a", "--action", type=str, help="backup or restore, default = backup", default="backup")
    restore_version = parser.add_mutually_exclusive_group()
    restore_version.add_argument("-r", "--restore", type=str, help="restore version, LATEST or date like 20190726, default = LATEST ", default="LATEST")
    restore_location = parser.add_mutually_exclusive_group()
    restore_location.add_argument("-l", "--location", type=str, help="restore from location, local, s3 or snapshot, default = LATEST ", default="local")

    args = parser.parse_args()
    print("parsed input: {}".format(args))

    svc = "neo4j"

    if args.action == "backup":
        backup(svc, statusService(), '/data/neo4j')
    elif args.action == "restore" and args.restore == "LATEST":
        restore(svc, args.restore, args.location)
    elif args.action == "restore" and args.restore != "LATEST":
        pass
    else:
        print("ERROR unknown input")

    # START service after backup or restore
    status = statusService()
    if status == 1:
        startService()
    elif status == 0:
        print('{} is {}'.format(svc, getState(0)))
    else:
        print('unknown status of {}'.format(status))
